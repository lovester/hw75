const express = require('express');
const crypt = require('./crypt');
const app = express();
const port = 8000;
const Vigenere = require('caesar-salad').Vigenere;

app.use(express.json());

app.use('/crypt', crypt);

app.listen(port, () => {
    console.log(`Server started on ${port} port`);
});